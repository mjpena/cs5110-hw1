import random

class Worm(object):
    def __init__(self, HEAD, cell_width, cell_height):
        self.head = HEAD
        self.cw = cell_width
        self.ch = cell_height
        self.startx = None
        self.starty = None
        self.x = None
        self.y = None

    def get_head(self):
        return self.head

    def set_worm_coord(self):
        self.startx = random.randint(5, self.cw - 6)
        self.starty = random.randint(5, self.ch - 6)
        wormCoords = [{ self.x : self.startx,       self.y : self.starty,
                        self.x : self.startx - 1,   self.y : self.starty - 1,
                        self.x: self.startx - 2, self.y: self.starty - 2}]
        direction = RIGHT


'''
startx = random.randint(5, CELLWIDTH - 6)
    starty = random.randint(5, CELLHEIGHT - 6)
    wormCoords = [{'x': startx,     'y': starty},
                  {'x': startx - 1, 'y': starty},
                  {'x': startx - 2, 'y': starty}]
    direction = RIGHT
'''