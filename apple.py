import random


class Apple(object):
    def __init__(self, cell_width, cell_height):
        self.cw = cell_width
        self.ch = cell_height
        self.x = None
        self.y = None

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def set_random_location(self):
        self.x = random.randint(0, self.cw - 1)
        self.y = random.randint(0, self.ch - 1)
