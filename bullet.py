class Bullet(object):
    def __init__(self, x, y, d, cw, ch):
        self.x = x
        self.y = y
        self.d = d
        self.cw = cw
        self.ch = ch

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_d(self):
        return self.d

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y
