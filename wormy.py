# Wormy (a Nibbles clone)
# By Al Sweigart al@inventwithpython.com
# http://inventwithpython.com/pygame
# Released under a "Simplified BSD" license

import random, pygame, sys
from pygame.locals import *
from apple import Apple
from bullet import Bullet


FPS = 2
WINDOWWIDTH = 1200
WINDOWHEIGHT = 600
CELLSIZE = 20
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)


#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
BLUE      = (  0,   0, 255)
YELLOW    = (255, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKBLUE  = (  0,   0, 155)
DARKGRAY  = ( 40,  40,  40)
LIGHTGRAY = (200, 200, 200)
BGCOLOR = BLACK

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

HEAD = 0  # syntactic sugar: index of the worm's head
HEAD_2 = 0  # second worm's head

APPLECOUNT = 5

def main():
    global FPSCLOCK, DISPLAYSURF, BASICFONT

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 18)
    pygame.display.set_caption('Snake')

    showStartScreen()
    while True:
        runGame()
        showGameOverScreen()


def runGame():
    # Set a random start point.
    startx = random.randint(5, CELLWIDTH - 6)
    starty = random.randint(5, CELLHEIGHT - 6)
    wormCoords = [{'x': startx,     'y': starty},
                  {'x': startx - 1, 'y': starty},
                  {'x': startx - 2, 'y': starty}]
    direction = RIGHT

    startx2 = random.randint(5, CELLWIDTH - 6)
    starty2 = random.randint(5, CELLHEIGHT - 6)
    worm2Coords = [{'x': startx2, 'y': starty2},
                  {'x': startx2 - 1, 'y': starty2},
                  {'x': startx2 - 2, 'y': starty2}]
    direction2 = RIGHT

    # Start the apple in a random place.
    apple_list = []
    for i in range(APPLECOUNT):
        apple = Apple(CELLWIDTH, CELLHEIGHT)
        apple.set_random_location()
        apple_list.append(apple)

    bullet_list = []


    while True: # main game loop
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()
            elif event.type == KEYDOWN:
                if (event.key == K_LEFT or event.key == K_KP6) and direction != RIGHT:
                    direction = LEFT
                elif (event.key == K_RIGHT or event.key == K_KP4) and direction != LEFT:
                    direction = RIGHT
                elif (event.key == K_UP or event.key == K_KP2) and direction != DOWN:
                    direction = UP
                elif (event.key == K_DOWN or event.key == K_KP8) and direction != UP:
                    direction = DOWN
                elif (event.key == K_l):
                    # worm1 shoots
                    bullet = Bullet(wormCoords[HEAD]['x'], wormCoords[HEAD]['y'], direction, CELLWIDTH, CELLHEIGHT)
                    bullet_list.append(bullet)
                elif (event.key == K_a or event.key == K_KP6) and direction2 != RIGHT:
                    direction2 = LEFT
                elif (event.key == K_d or event.key == K_KP4) and direction2 != LEFT:
                    direction2 = RIGHT
                elif (event.key == K_w or event.key == K_KP2) and direction2 != DOWN:
                    direction2 = UP
                elif (event.key == K_s or event.key == K_KP8) and direction2 != UP:
                    direction2 = DOWN
                elif (event.key == K_q):
                    # worm2 shoots
                    bullet = Bullet(worm2Coords[HEAD]['x'], worm2Coords[HEAD]['y'], direction2, CELLWIDTH, CELLHEIGHT)
                    bullet_list.append(bullet)
                elif event.key == K_ESCAPE:
                    terminate()


        # check if the worm has hit the edge
        if wormCoords[HEAD]['x'] == -1 or wormCoords[HEAD]['x'] == CELLWIDTH or wormCoords[HEAD]['y'] == -1 or wormCoords[HEAD]['y'] == CELLHEIGHT:
            return  # game over
        if worm2Coords[HEAD]['x'] == -1 or worm2Coords[HEAD]['x'] == CELLWIDTH or worm2Coords[HEAD]['y'] == -1 or worm2Coords[HEAD]['y'] == CELLHEIGHT:
            return  # game over

        # check if worm has hit itself
        for wormBody in wormCoords[1:]:
            if wormBody['x'] == wormCoords[HEAD]['x'] and wormBody['y'] == wormCoords[HEAD]['y']:
                return  # game over
        for wormBody in worm2Coords[1:]:
            if wormBody['x'] == worm2Coords[HEAD]['x'] and wormBody['y'] == worm2Coords[HEAD]['y']:
                return  # game over

        # check if worm1 has hit worm2
        for worm2Body in worm2Coords[1:]:
            if wormCoords[HEAD]['x'] == worm2Body['x'] and wormCoords[HEAD]['y'] == worm2Body['y']:
                return  # game over

        # check if worm2 has hit worm1
        for wormBody in wormCoords[1:]:
            if worm2Coords[HEAD]['x'] == wormBody['x'] and worm2Coords[HEAD]['y'] == wormBody['y']:
                return  # game over

        # check if worm2 has been hit by bullet
        for worm2Body in worm2Coords[1:]:
            for i in range(len(bullet_list)):
                if bullet_list[i].get_x() == worm2Body['x'] and bullet_list[i].get_y() == worm2Body['y']:
                    return  # game over

        # check if worm1 has been hit by bullet
        for wormBody in wormCoords[1:]:
            for i in range(len(bullet_list)):
                if bullet_list[i].get_x() == wormBody['x'] and bullet_list[i].get_y() == wormBody['y']:
                    return  # game over

        # check if worm has eaten an apple
        ate_apple = False
        for i in range(len(apple_list)):
            if wormCoords[HEAD]['x'] == apple_list[i].get_x() and wormCoords[HEAD]['y'] == apple_list[i].get_y():
                # don't remove worm's tail segment
                apple_list[i].set_random_location()  # set a new apple somewhere
                ate_apple = True
        if not ate_apple:
            del wormCoords[-1]  # remove worm's tail segment

        ate_apple = False
        for i in range(len(apple_list)):
            if worm2Coords[HEAD]['x'] == apple_list[i].get_x() and worm2Coords[HEAD]['y'] == apple_list[i].get_y():
                # don't remove worm's tail segment
                apple_list[i].set_random_location()  # set a new apple somewhere
                ate_apple = True
        if not ate_apple:
            del worm2Coords[-1]  # remove worm's tail segment

        # move the worm by adding a segment in the direction it is moving
        if direction == UP:
            newHead = {'x': wormCoords[HEAD]['x'], 'y': wormCoords[HEAD]['y'] - 1}
        elif direction == DOWN:
            newHead = {'x': wormCoords[HEAD]['x'], 'y': wormCoords[HEAD]['y'] + 1}
        elif direction == LEFT:
            newHead = {'x': wormCoords[HEAD]['x'] - 1, 'y': wormCoords[HEAD]['y']}
        elif direction == RIGHT:
            newHead = {'x': wormCoords[HEAD]['x'] + 1, 'y': wormCoords[HEAD]['y']}

        # second worm moving
        if direction2 == UP:
            newHead2 = {'x': worm2Coords[HEAD]['x'], 'y': worm2Coords[HEAD]['y'] - 1}
        elif direction2 == DOWN:
            newHead2 = {'x': worm2Coords[HEAD]['x'], 'y': worm2Coords[HEAD]['y'] + 1}
        elif direction2 == LEFT:
            newHead2 = {'x': worm2Coords[HEAD]['x'] - 1, 'y': worm2Coords[HEAD]['y']}
        elif direction2 == RIGHT:
            newHead2 = {'x': worm2Coords[HEAD]['x'] + 1, 'y': worm2Coords[HEAD]['y']}

        # bullet moving
        for i in range(len(bullet_list)):
            if bullet_list[i].get_d() == UP:
                bullet_list[i].set_y( bullet_list[i].get_y() - 2)
            elif bullet_list[i].get_d() == DOWN:
                bullet_list[i].set_y(bullet_list[i].get_y() + 2)
            elif bullet_list[i].get_d() == LEFT:
                bullet_list[i].set_x(bullet_list[i].get_x() - 2)
            elif bullet_list[i].get_d() == RIGHT:
                bullet_list[i].set_x(bullet_list[i].get_x() + 2)


        wormCoords.insert(0, newHead)
        worm2Coords.insert(0, newHead2)
        DISPLAYSURF.fill(BGCOLOR)
        drawGrid()
        drawWorm(wormCoords, 1)
        drawWorm(worm2Coords, 2)
        drawScore(len(wormCoords) - 3, 1)
        drawScore(len(worm2Coords) - 3, 2)
        for i in range(len(apple_list)):
            drawApple(apple_list[i])
        for i in range(len(bullet_list)):
            drawBullet(bullet_list[i])
        pygame.display.update()
        FPSCLOCK.tick(FPS)

def drawPressKeyMsg():
    pressKeySurf = BASICFONT.render('Press a key to play.', True, DARKGRAY)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topleft = (WINDOWWIDTH - 200, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)


def checkForKeyPress():
    if len(pygame.event.get(QUIT)) > 0:
        terminate()

    keyUpEvents = pygame.event.get(KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == K_ESCAPE:
        terminate()
    return keyUpEvents[0].key


def showStartScreen():
    titleFont = pygame.font.Font('freesansbold.ttf', 100)
    titleSurf1 = titleFont.render('Snake!', True, BLACK, RED)
    titleSurf2 = titleFont.render('Snake!', True, DARKGRAY)

    degrees1 = 0
    degrees2 = 0
    while True:
        DISPLAYSURF.fill(BGCOLOR)
        rotatedSurf1 = pygame.transform.rotate(titleSurf1, degrees1)
        rotatedRect1 = rotatedSurf1.get_rect()
        rotatedRect1.center = (WINDOWWIDTH / 2, WINDOWHEIGHT / 2)
        DISPLAYSURF.blit(rotatedSurf1, rotatedRect1)

        rotatedSurf2 = pygame.transform.rotate(titleSurf2, degrees2)
        rotatedRect2 = rotatedSurf2.get_rect()
        rotatedRect2.center = (WINDOWWIDTH / 2, WINDOWHEIGHT / 2)
        DISPLAYSURF.blit(rotatedSurf2, rotatedRect2)

        drawPressKeyMsg()

        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return
        pygame.display.update()
        FPSCLOCK.tick(FPS)
        degrees1 += 3 # rotate by 3 degrees each frame
        degrees2 += 7 # rotate by 7 degrees each frame


def terminate():
    pygame.quit()
    sys.exit()


def getRandomLocation():
    return {'x': random.randint(0, CELLWIDTH - 1), 'y': random.randint(0, CELLHEIGHT - 1)}


def showGameOverScreen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midtop = (WINDOWWIDTH / 2, 10)
    overRect.midtop = (WINDOWWIDTH / 2, gameRect.height + 10 + 25)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    drawPressKeyMsg()
    pygame.display.update()
    pygame.time.wait(500)
    checkForKeyPress() # clear out any key presses in the event queue

    while True:
        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return

def drawScore(score, snake_num):
    if snake_num == 1:
        scoreSurf = BASICFONT.render('Score: %s' % (score), True, GREEN)
        scoreRect = scoreSurf.get_rect()
        scoreRect.topleft = (WINDOWWIDTH - 120, 10)
        DISPLAYSURF.blit(scoreSurf, scoreRect)
    else:
        scoreSurf = BASICFONT.render('Score: %s' % (score), True, BLUE)
        scoreRect = scoreSurf.get_rect()
        scoreRect.topleft = (WINDOWWIDTH - 200, 10)
        DISPLAYSURF.blit(scoreSurf, scoreRect)

def drawWorm(wormCoords, worm_num):
    for coord in wormCoords:
        x = coord['x'] * CELLSIZE
        y = coord['y'] * CELLSIZE
        wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
        if worm_num == 1:
            pygame.draw.rect(DISPLAYSURF, DARKGREEN, wormSegmentRect)
        else:
            pygame.draw.rect(DISPLAYSURF, DARKBLUE, wormSegmentRect)
        wormInnerSegmentRect = pygame.Rect(x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
        if worm_num == 1:
            pygame.draw.rect(DISPLAYSURF, GREEN, wormInnerSegmentRect)
        else:
            pygame.draw.rect(DISPLAYSURF, BLUE, wormInnerSegmentRect)

def drawApple(app):
    x = app.get_x() * CELLSIZE
    y = app.get_y() * CELLSIZE
    appleRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    pygame.draw.rect(DISPLAYSURF, RED, appleRect)

def drawBullet(bul):
    x = bul.get_x() * CELLSIZE
    y = bul.get_y() * CELLSIZE
    bulRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    pygame.draw.rect(DISPLAYSURF, YELLOW, bulRect)

def drawGrid():
    for x in range(0, WINDOWWIDTH, CELLSIZE): # draw vertical lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE): # draw horizontal lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))


if __name__ == '__main__':
    main()